export interface Movie
{
  id: number;
  title: string;
  category: string;
  releaseYear: number;
  poster: string;
  directors: string;
  actors: [];
  synopsis: string;
  rate: number;
  lastViewDate: Date;
  price: number;
}
