import { Component, OnInit } from '@angular/core';
import {Movieservice} from './service/Movie.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private movieService: Movieservice,
    private  router: Router,
  ) { }

  ngOnInit(): void {
  }

}
