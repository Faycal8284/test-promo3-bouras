import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
const moviesDB = 'http://localhost:3035/api/';
@Injectable({
  providedIn: 'root'
})
export class Movieservice {
  constructor(private http: HttpClient ) {
  }
  getAll(): Observable<any> {
    return this.http.get(moviesDB);
  }

  get(id: number): Observable<any> {
    return this.http.get(`${moviesDB}/${id}`);
  }

  create(data: any): Observable<any> {
    return this.http.post(moviesDB, data);
  }

  update(id: number, data: any): Observable<any> {
    return this.http.put(`${moviesDB}/${id}`, data);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${moviesDB}/${id}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(moviesDB);
  }

  findByTitle(title: string): Observable<any> {
    return this.http.get(`${moviesDB}?title=${title}`);
  }
}
